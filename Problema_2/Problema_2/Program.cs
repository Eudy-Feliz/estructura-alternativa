﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.Write("ingrese el numero (a) ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.Write("ingrese el numero (b) ");
            b = Convert.ToInt32(Console.ReadLine());

            if (a % b == 0)
            {
                Console.WriteLine("el numero a : {0}, es multiplo de b : {1} ", a, b);
            }
            else
            {
                Console.WriteLine("el numero a : {0}, no es multiplo de b : {1} ", a, b);
            }

            Console.ReadLine();
        }
    }
}
