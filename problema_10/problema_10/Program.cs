﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problema_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese un numero entero: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese otro numero entero: ");
            int num2 = int.Parse(Console.ReadLine());

            if (num1 < num2)
            {
                Console.WriteLine("El primer numero es el menor: " + num1);
            }
            else
            {
                Console.WriteLine("El segundo numero es el menor: " + num2);
            }

            Console.ReadLine();

        }
    }
}
