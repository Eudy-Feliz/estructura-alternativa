﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problema_8
{
    class Program
    {
        static void Main(string[] args)
        {
            char caracter;

            Console.WriteLine("digite un caracter");
            caracter = Convert.ToChar(Console.ReadLine());
            if (char.IsLetter(caracter))
            {
                Console.WriteLine(caracter + ": es letra");
            }
            else if (char.IsPunctuation(caracter))
            {
                Console.WriteLine(caracter + ": es un signo de puntuacion");
            }

            else if (char.IsSeparator(caracter))
            {
                Console.WriteLine(caracter + ": es un separador");
            }
            else if (char.IsDigit(caracter))
            {
                Console.WriteLine(caracter + ": es numero");
            }
            else if (char.IsSymbol(caracter))
            {
                Console.WriteLine(caracter + ": es un simbolo");
            }
            else
            {
                Console.WriteLine(" es un caracter especial ");
            }

            Console.ReadKey();
        }
    }
}
